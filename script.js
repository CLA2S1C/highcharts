$(function () {
    Highcharts.chart('container', {
        chart: {
            type: 'column',
            marginTop: 60,
            events: {
                load: function(event) {
                    $('.highcharts-legend-item rect').attr('height', '7').attr('y', '7');
                }
            },
        },
        title: {
            text: 'TSO - Overall Pipeline Inspection & Maintenance Achievement (PM) (May 2020)'
        },
        xAxis: {
            categories: ['R1', 'R2', 'R3', 'R4', 'R5', 'R6', 'R7', 'R8', 'R9', 'R10', 'R11', 'R12', 'OFFSHORE', 'OSP', 'GSP Rayong'],
            title: {
                enabled: false
            },
            gridLineColor: 'rgb(201, 129, 22)',
            gridLineWidth: 2,
            gridLineDashStyle: 'ShortDash'
        },
        yAxis: {
            labels: {
                format: '{value}%'
            },
            title: {
                enabled: false
            },
            max: 100
        },
        plotOptions: {
            series: {
                groupPadding: 0.05,
            },
        },
        legend: {
            //symbolHeight: 11,
            //align: 'left',
            //verticalAlign: 'middle',
            //layout: 'horizontal',
            //floating:false,
            //borderWidth: 1,
            //itemWidth: 400,
            //symbolPadding: 10,
            itemDistance: 5,
            padding: 0,
            symbolWidth: 30,
            symbolRadius: 0,
            align: 'center',
          },
        tooltip: {
            headerFormat: '<span style="font-size:11px">Series "{series.name}" Point "{point.key}"</span><br>',
            pointFormat: '<span style="font-size:11px;color:{point.color}">Value: <b>{point.y:.2f}%</b>'
        },
        series: [{
            name: 'P/S Potential',
            data: [100,100,100,100,100,82,100,0,100,100,100,100,0,0,0],
            color: 'rgb(5, 167, 40)',
            marker: {
                symbol: 'url(https://www.highcharts.com/samples/graphics/sun.png)'
              }
        }, {
            name: 'Rectifier',
            data: [94,100,100,100,100,100,100,100,100,100,100,100,0,0,0],
            color: 'rgb(5, 167, 159)',
        }, {
            name: 'Bond Box',
            data: [100,100,100,100,100,100,100,100,100,100,100,100,0,0,0],
            color: 'rgb(5, 89, 167)'
        }, {
            name: 'IJ/IF/DC Decoupler',
            data: [100,26,71,100,100,100,100,100,100,100,0,100,0,0,100],
            color: 'rgb(0, 114, 51)'
        }, {
            name: 'Anode',
            data: [100,100,100,100,100,100,100,100,100,100,100,0,0,0,100],
            color: 'rgb(0, 87, 114)'
        }, {
            name: 'CIPS/DCVG',
            data: [0,0,100,100,100,100,0,0,100,0,0,0,0,0,0],
            color: 'rgb(0, 125, 163)'
        }, {
            name: 'ROV',
            data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        }, {
            name: 'Settlement Monitor',
            data: [100,100,0,0,100,100,0,0,100,100,100,100,0,0,0],
            color: 'rgb(36, 195, 243)',
        }, {
            name: 'Patrolling/Vault inspection',
            data: [100,100,100,100,100,100,100,100,100,100,100,100,0,0,0],
            color: 'rgb(0, 87, 114)'
        }, {
            name: 'Cleaning PIG',
            data: [100,100,67,0,0,0,100,0,100,100,100,0,67,0,0],
            color: 'rgb(0, 114, 51)'
        }, {
            name: 'ILI',
            data: [100,100,100,0,0,0,100,0,100,100,100,0,100,0,0],
            color: 'rgb(5, 89, 167)'
        }, {
            name: 'RBI',
            data: [100,100,100,100,0,100,0,100,0,0,0,100,100,100,0],
            color: 'rgb(5, 167, 159)'
        }, {
            name: 'Visual (Soil to Air/CUS/CUI/Splash zone)',
            data: [100,100,100,100,100,100,100,100,100,100,100,100,100,100,100],
            color: 'rgb(0, 125, 163)'
        },{
            type: 'spline',
            name: 'Overall',
            dashStyle: 'ShortDashDot',
            data: [99,85,94,100,100,98,100,88,100,100,82,89,92,100,38],
            marker: {
                symbol: 'diamond',
                lineWidth: 1,
                lineColor: "rgb(1, 88, 70)",
                fillColor: 'rgb(1, 88, 70)',
            },
            dataLabels: {
                enabled: true,
                borderRadius: 1,
                backgroundColor: 'rgba(227, 238, 20, 0.7)',
                borderWidth: 1,
                borderColor: '#AAA',
                y: -6,
                overflow: 'none',
                crop: false
            },
            color: 'rgb(3, 204, 164)'
        },]
    });
    // Define a custom symbol path
    Highcharts.SVGRenderer.prototype.symbols.minus = function (x, y, w, h) {
        return ['M', x, y + h / 2, 'L', x + w, y + h / 2];
    };
    if (Highcharts.VMLRenderer) {
        Highcharts.VMLRenderer.prototype.symbols.minus = Highcharts.SVGRenderer.prototype.symbols.minus;
    }
    Highcharts.chart('stacked',{
        chart: {
            type: 'column',
            marginTop: 80,
            plotBorderWidth: 1
        },
        title: {
            text: 'NGV Patrolling 2019'
        },
        xAxis: {
            categories: ['R1', 'R2', 'R3', 'R4', 'R5', 'R6', 'R7', 'R8', 'R9', 'R10', 'R11', 'R12'],
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Percentage'
            },
            stackLabels: {
                enabled: false,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'black'
                }
            },
            reversedStacks: false,
            gridLineColor: 'rgb(150, 150, 150)',
            gridLineWidth: 0.5,
        },
        legend: {
            align: 'center',
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
            shadow: false,
            symbolRadius: 0,
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'percent',
                dataLabels: {
                    enabled: false
                },
                //pointPadding: 0,
                //groupPadding: 0,
                borderWidth: 0
            },
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">Series "{series.name}" Point "{point.key}"</span><br>',
            pointFormat: '<span style="font-size:11px;color:{point.color}">Value: <b>{point.y:.1f}</b>'
        },
        series: [{
            name: 'Actual',
            index: 2,
            data: [48.2,25.2,66.6,23.1,15.5,58.4,19.2,20.0,34.7,15.6,15.6,46.7],
            color: 'rgb(95, 211, 0)',
            dataLabels: {
                enabled: true,
                formatter: function () {
                    return Highcharts.numberFormat(this.y,0);
                }
            },
        }, {
            name: 'Plan',
            data: [100,100,100,100,100,100,100,100,100,100,100,100],
            color: 'rgb(0, 109, 211)'
        }, {
            name: 'Postpone',
            data: [0,0,0,0,0,0,0,0,0,0,0]
        }, {
            name: 'Q1',
            type: 'scatter',
            dataLabels: {
                enabled: true,
                align: 'left',
                format: '{series.name}',
            },
            data: [21.77419355,23.13432836,26.8707483,23.07692308,27.39726027,22.4137931,23.07692308,20,25.2293578,26.66666667,26.66666667,2],
            showInLegend: false,
            marker: {
                symbol: 'minus',
                lineColor:'#000',
                lineWidth: 4,
                radius: 11.5,
            },
            align: 'center',
        }, {
            name: 'Q2',
            type: 'scatter',
            dataLabels: {
                enabled: true,
                align: 'left',
                format: '{series.name}',
            },
            data: [46.37096774,53.35820896,52.72108844,53.84615385,53.42465753,53.44827586,53.84615385,46.66666667,50.4587156,53.33333333,53.33333333,46.66666667],
            showInLegend: false,
            marker: {
                symbol: 'minus',
                lineColor:'#ff7300',
                lineWidth: 4,
                radius: 11.5,
            },
            align: 'center',
        }, {
            name: 'Q3',
            type: 'scatter',
            dataLabels: {
                enabled: true,
                align: 'left',
                format: '{series.name}',
            },
            data: [68.5483871,73.50746269,76.19047619,76.92307692,73.97260274,74.13793103,76.92307692,66.66666667,74.31192661,73.33333333,73.33333333,73.33333333],
            showInLegend: false,
            marker: {
                symbol: 'minus',
                lineColor:'#000',
                lineWidth: 4,
                radius: 11.5,
            },
            align: 'center',
        }, {
            name: 'Q4',
            type: 'scatter',
            dataLabels: {
                enabled: true,
                align: 'left',
                format: '{series.name}',
                overflow: 'none',
                crop: false
            },
            data: [100,100,100,100,100,100,100,100,100,100,100,100],
            showInLegend: false,
            marker: {
                symbol: 'minus',
                lineColor:'#000',
                lineWidth: 4,
                radius: 11.5,
            },
            align: 'center',
        }]
    });

});